import React, { useEffect, useState } from 'react'
import Api from 'api'
import MapChart from './components/MapCharts/MapChart'
import * as css from './App.css'
import { ListMetaData } from './type'
import { Center } from './components/Center/Center'

const api = new Api()

export const dataTestIds = {
    root: 'app',
}

function App() {
    const [data, setData] = useState<ListMetaData>([])

    useEffect(() => {
        api.getList().then(setData)
    }, [])

    return (
        <div data-testid={dataTestIds.root}>
            <Center column>
                <div>Wind for life</div>
                <div className={css.map__chart__container}>
                    {<MapChart data={data} />}
                </div>
            </Center>
        </div>
    )
}

export default App
