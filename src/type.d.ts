import { Point } from 'react-simple-maps'

type MetaData = {
    id: number
    name: string
    loc: { lat: number; long: number }
}

type WindData = {
    timestamp: string
    force: number
    dir: number
}

type WindStatistics = {
    statistics: {
        average: {
            daily: {
                force: number
            }
            weekly: { force: number }
        }
    }
}

export type ListMetaData = MetaData[]

export type WindListItem = MetaData & WindStatistics & { readings: WindData[] }

export type WindListItems = WindListItem[]

export type Markers = Array<{
    markerOffset: number
    name: string
    coordinates: Point
}>
