import React from 'react'
import {
    ComposableMap,
    Geographies,
    Geography,
    Marker,
    ZoomableGroup,
} from 'react-simple-maps'
import { ListMetaData } from '../../type'
import { formatListToMarkers } from '../../helpers/format'
import geoUrl from './hawaii.json'

interface MapChartProps {
    data: ListMetaData
}

const MapChart = ({ data }: MapChartProps) => {
    return (
        <ComposableMap
            projection="geoAlbersUsa"
            // projectionConfig={{
            //     rotate: [-10, 0, 0],
            //     scale: 147,
            // }}
        >
            <ZoomableGroup center={[0, 0]} zoom={1}>
                <Geographies geography={geoUrl}>
                    {({ geographies }) => {
                        return geographies.map((geo) => (
                            <Geography
                                key={geo.rsmKey}
                                geography={geo}
                                fill="#EAEAEC"
                                stroke="black"
                            />
                        ))
                    }}
                </Geographies>
                {formatListToMarkers(data).map(
                    ({ name, coordinates, markerOffset }) => (
                        <Marker
                            onClick={() => console.log('clicked')}
                            key={name}
                            coordinates={coordinates}
                        >
                            <g
                                width={2}
                                height={2}
                                cursor="pointer"
                                fill="transparent"
                                stroke="#FF5533"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                transform="translate(-12, -24)"
                            >
                                <circle cx="12" cy="10" r="3" />
                                <path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z" />
                            </g>
                            <text
                                fontSize={9}
                                textAnchor="middle"
                                y={markerOffset}
                                style={{
                                    fontFamily: 'system-ui',
                                    fill: '#5D5A6D',
                                }}
                            >
                                {name}
                            </text>
                        </Marker>
                    )
                )}
            </ZoomableGroup>
        </ComposableMap>
    )
}

export default MapChart
