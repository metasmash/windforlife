import { css } from 'emotion'

export const center = css`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const column = css`
    flex-direction: column;
`
