import React from 'react'
import classNames from 'classnames'
import { ReactNode } from 'react'
import * as css from './Center.css'

export const Center = ({
    children,
    column = false,
}: {
    children: ReactNode
    column?: boolean
}) => (
    <div className={classNames(css.center, { [css.column]: column })}>
        {children}
    </div>
)
