import { WindListItems } from '../../type'
import data_1 from './1.json'
import data_2 from './2.json'
import data_3 from './3.json'

const items: WindListItems = [data_1, data_2, data_3]

export default items
