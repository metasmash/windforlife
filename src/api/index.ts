import _ from 'lodash'
import { ListMetaData, WindListItem } from '../type'
import list from './list.json'
import items from './detail'

export default class API {
    async getList(): Promise<ListMetaData> {
        return new Promise((resolve) => {
            setTimeout(function () {
                resolve(list)
            }, 1000)
        })
    }

    async getDataById(id: number): Promise<WindListItem> {
        return new Promise((resolve) => {
            setTimeout(function () {
                resolve(_.find(items, (item) => item.id === id))
            }, 1000)
        })
    }
}
