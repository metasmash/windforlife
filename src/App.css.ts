import { css } from 'emotion'

export const map__chart__container = css`
    width: 500px;
    border: 1px solid black;
    cursor: grab;
    &:active {
        cursor: grabbing;
    }
`
