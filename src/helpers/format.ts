import _ from 'lodash'
import { ListMetaData, Markers } from '../type'

export const formatListToMarkers = (data: ListMetaData): Markers =>
    _.reduce(
        data,
        (final: Markers, current) => [
            ...final,
            {
                markerOffset: -30,
                name: current.name,
                coordinates: [current.loc.long, current.loc.lat],
            },
        ],
        []
    )
