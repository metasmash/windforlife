import React from 'react'
import { render } from '@testing-library/react'
import App, { dataTestIds } from './App'

const getContent = () => render(<App />)

describe('App', () => {
    it('Should render without crashing', () => {
        const { queryByTestId } = getContent()

        expect(queryByTestId(dataTestIds.root)).toBeTruthy()
    })
})
